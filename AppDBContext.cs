﻿using Microsoft.EntityFrameworkCore;
using Ecommerce.Enities;

public class AppDBContext : DbContext
{
    public AppDBContext(DbContextOptions<AppDBContext> options) : base(options) { }
    public DbSet<Product> Products { get; set; }
    public DbSet<Product> Customer { get; set; }
    public DbSet<Product> OrderItems { get; set; }
    public DbSet<Product> Customers { get; set; }

}
