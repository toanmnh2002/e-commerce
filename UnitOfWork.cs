﻿using Ecommerce.IRepository;
using Ecommerce.Repository;

namespace Ecommerce
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDBContext _dbContext;
        private readonly IProductRepository _productRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly ICustomerRepository _customerRepository;

        public UnitOfWork(AppDBContext dbContext)
        {
            _dbContext = dbContext;
            _productRepository = new ProductRepository(_dbContext);
            _orderRepository=new OrderRepository(_dbContext);
            _customerRepository=new CustomerRepository(_dbContext);
        }
        public IProductRepository ProductRepository => _productRepository;
        public IOrderRepository OrderRepository => _orderRepository;
        public ICustomerRepository CustomerRepository => _customerRepository;
        public async Task<int> SaveChangeAsync() => await _dbContext.SaveChangesAsync();
    }
}
