﻿using Ecommerce.IRepository;

namespace Ecommerce
{
    public interface IUnitOfWork
    {
        public IProductRepository ProductRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public ICustomerRepository CustomerRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
