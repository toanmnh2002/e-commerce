﻿using Ecommerce.DTO;
using Ecommerce.Entities;
using Ecommerce.IService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }
        [HttpPost]
        public async Task<IActionResult> AddCustomer([FromBody] CustomerDTO customer)
        {
            var obj = new Customer()
            {
                FullName = customer.FullName,
                Address = customer.Address,
                PhoneNumber = customer.PhoneNumber
            };
            await _customerService.AddCustomer(obj);
            return Ok(obj);
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCustomer([FromRoute] int id, [FromBody] CustomerDTO customer)
        {
            var obj = await _customerService.GetCustomer(id);
            if (obj is not null)
            {
                obj.FullName = customer.FullName;
                obj.Address = customer.Address;
                obj.PhoneNumber = customer.PhoneNumber;
                await _customerService.UpdateCustomer(obj);
                return Ok(obj);
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            await _customerService.DeleteCustomer(id);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomerById(int id)
        {
            var customer = await _customerService.GetCustomer(id);
            return Ok(customer);
        }
        [HttpGet]
        public async Task<IActionResult> GetCustomerAsync()
        {
            var listCustomer = await _customerService.ViewAllCustomer();
            return Ok(listCustomer);
        }
    }
}
