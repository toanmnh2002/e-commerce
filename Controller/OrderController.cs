﻿using Ecommerce.DTO;
using Ecommerce.Entities;
using Ecommerce.IService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        public async Task<IActionResult> AddOrder([FromBody] OrderDTO order)
        {
            var obj = new Order()
            {
                OrderDate = order.OrderDate = DateTime.Now,
                TotalPrice = order.TotalPrice,
                CustomerId = order.CustomerId
            };
            await _orderService.AddOrder(obj);
            return Ok(obj);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOrder([FromRoute] int id, [FromBody] OrderDTO order)
        {
            var obj = await _orderService.GetOrder(id);
            if (obj is not null)
            {
                obj.OrderDate = order.OrderDate;
                obj.TotalPrice = order.TotalPrice;
                obj.CustomerId = order.CustomerId;
                await _orderService.UpdateOrder(obj);
                return Ok(obj);
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            await _orderService.DeleteOrder(id);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrderById(int id)
        {
            var order = await _orderService.GetOrder(id);
            return Ok(order);
        }
        [HttpGet]
        public async Task<IActionResult> GetOrderAsync()
        {
            var listOrder = await _orderService.ViewAllOrder();
            return Ok(listOrder);
        }
    }
}
