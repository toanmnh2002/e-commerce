﻿using Microsoft.AspNetCore.Mvc;
using Ecommerce.DTO;
using Ecommerce.Enities;
using Ecommerce.IService;

namespace Ecommerce.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        [HttpPost]
        public async Task<IActionResult> AddProduct([FromBody] ProductDTO product)
        {
            var obj = new Product()
            {
                Name = product.Name,
                Price = product.Price
            };
            await _productService.AddProduct(obj);
            return Ok(obj);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProduct([FromRoute] int id, [FromBody] ProductDTO product)
        {
            var obj = await _productService.GetProduct(id);
            if (obj is not null)
            {
                obj.Price = product.Price;
                obj.Name = product.Name;
                await _productService.UpdateProduct(obj);
                return Ok(obj);
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            await _productService.DeleteProduct(id);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductById(int id)
        {
            var Product = await _productService.GetProduct(id);
            return Ok(Product);
        }
        [HttpGet]
        public async Task<IActionResult> GetProductAsync()
        {
            var listProduct = await _productService.ViewAllProduct();
            return Ok(listProduct);
        }
    }
}
