﻿using Ecommerce.DTO;
using Ecommerce.Entities;
using Ecommerce.IService;

namespace Ecommerce.Service
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task AddCustomer(Customer customer)
        {
            await _unitOfWork.CustomerRepository.AddEntityAsync(customer);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Add Successfully!");
            }
            else
            {
                Console.WriteLine("Add Failed!");
            }
        }
        //public async Task AddOrdersToCustomer(OrderMapper orderMapper)
        //{
        //    var obj = await _unitOfWork.CustomerRepository.GetEntityByIdAsync(CustomerId);
        //    var list=new List<Order>();
        //    for (int i = 0; i < orderMapper.Orders.Count; i++)
        //    {
        //        list.Add(new OrderMapper
        //        {
        //            OrderId=orderMapper.OrderId,

        //        }) ;
        //    }

        //    if (obj is not null)
        //    {
        //        Order o = new Order();
        //        o.Id = order.OrderId;
        //        o.CustomerId=CustomerId;
        //        o.TotalPrice=order.TotalPrice;
        //        o.OrderDate=order.OrderDate;
        //        await _unitOfWork.OrderRepository.AddEntityAsync(o);
        //    }
        //    var isSuccess = await _unitOfWork.SaveChangeAsync();
        //    if (isSuccess > 0)
        //    {
        //        Console.WriteLine("Add Successfully!");
        //    }
        //    else
        //    {
        //        Console.WriteLine("Add Failed!");
        //    }
        //}



        public async Task DeleteCustomer(int id)
        {
            var customer = await _unitOfWork.CustomerRepository.GetEntityByIdAsync(id);
            if (customer is not null)
            {
                _unitOfWork.CustomerRepository.SoftRemoveEntity(customer);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    Console.WriteLine("Delete Successfully!");
                }
                else
                {
                    Console.WriteLine("Delete Failed!");
                }
            }
            else
            {
                Console.WriteLine("Id not exist!");
            }
        }
        public async Task<Customer?> GetCustomer(int id) => await _unitOfWork.CustomerRepository.GetEntityByIdAsync(id);
        public async Task UpdateCustomer(Customer customer)
        {
            _unitOfWork.CustomerRepository.UpdateEntity(customer);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Update Successfully!");
            }
            else
            {
                Console.WriteLine("Update Failed!");
            }
        }
        public async Task<IEnumerable<Customer>> ViewAllCustomer()
        {
            return await _unitOfWork.CustomerRepository.GetListAsync();
        }

    }
}

