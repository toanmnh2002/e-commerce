﻿using Ecommerce.Entities;
using Ecommerce.IService;

namespace Ecommerce.Service
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        public OrderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task AddOrder(Order order)
        {
            await _unitOfWork.OrderRepository.AddEntityAsync(order);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Add Successfully!");
            }
            else
            {
                Console.WriteLine("Add Failed!");
            }
        }
        public async Task DeleteOrder(  int id)
        {
            var order = await _unitOfWork.OrderRepository.GetEntityByIdAsync(id);
            if (order is not null)
            {
                _unitOfWork.OrderRepository.SoftRemoveEntity(order);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    Console.WriteLine("Delete Successfully!");
                }
                else
                {
                    Console.WriteLine("Delete Failed!");
                }
            }
            else
            {
                Console.WriteLine("Id not exist!");
            }
        }
        public async Task<Order?> GetOrder(int id) => await _unitOfWork.OrderRepository.GetEntityByIdAsync(id);
        public async Task UpdateOrder(Order order)
        {
            _unitOfWork.OrderRepository.UpdateEntity(order);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Update Successfully!");
            }
            else
            {
                Console.WriteLine("Update Failed!");
            }
        }
        public async Task<IEnumerable<Order>> ViewAllOrder()
        {
            return await _unitOfWork.OrderRepository.GetListAsync();
        }

    }
}
