﻿using Ecommerce;
using Ecommerce.Enities;
using Ecommerce.IService;

public class ProductService : IProductService
{
    private readonly IUnitOfWork _unitOfWork;
    public ProductService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    public async Task AddProduct(Product product)
    {
        await _unitOfWork.ProductRepository.AddEntityAsync(product);
        var isSuccess = await _unitOfWork.SaveChangeAsync();
        if (isSuccess > 0)
        {
            Console.WriteLine("Add Successfully!");
        }
        else
        {
            Console.WriteLine("Add Failed!");
        }
    }
    public async Task DeleteProduct(int id)
    {
        var product = await _unitOfWork.ProductRepository.GetEntityByIdAsync(id);
        if (product is not null)
        {
            _unitOfWork.ProductRepository.SoftRemoveEntity(product);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Delete Successfully!");
            }
            else
            {
                Console.WriteLine("Delete Failed!");
            }
        }
        else
        {
            Console.WriteLine("Id not exist!");
        }
    }
    public async Task UpdateProduct(Product product)
    {
        _unitOfWork.ProductRepository.UpdateEntity(product);
        var isSuccess = await _unitOfWork.SaveChangeAsync();
        if (isSuccess > 0)
        {
            Console.WriteLine("Update Successfully!");
        }
        else
        {
            Console.WriteLine("Update Failed!");
        }
    }
    public async Task<Product?> GetProduct(int id) => await _unitOfWork.ProductRepository.GetEntityByIdAsync(id);
    public async Task<IEnumerable<Product>> ViewAllProduct()
    {
        return await _unitOfWork.ProductRepository.GetListAsync();
    }
}