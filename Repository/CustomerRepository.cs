﻿using Ecommerce.Entities;
using Ecommerce.IRepository;

namespace Ecommerce.Repository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }
    }
}
