﻿using Ecommerce.Entities;
using Ecommerce.IRepository;

namespace Ecommerce.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }
    }
}
