﻿using Ecommerce.Enities;

namespace Ecommerce.IService
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> ViewAllProduct();
        Task AddProduct(Product product);
        Task UpdateProduct(Product product);
        Task DeleteProduct(int id);
        Task<Product?> GetProduct(int id);
    }
}
