﻿using Ecommerce.DTO;
using Ecommerce.Entities;

namespace Ecommerce.IService
{
    public interface ICustomerService
    {
        Task<IEnumerable<Customer>> ViewAllCustomer();
        Task AddCustomer(Customer customer);
        Task UpdateCustomer(Customer customer);
        Task DeleteCustomer(int id);
        Task<Customer?> GetCustomer(int id);
        Task AddOrdersToCustomer(int id, OrderMapper order);

    }
}
