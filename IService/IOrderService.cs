﻿using Ecommerce.Entities;

namespace Ecommerce.IService
{
    public interface IOrderService
    {
        Task<IEnumerable<Order>> ViewAllOrder();
        Task AddOrder(Order order);
        Task UpdateOrder(Order order);
        Task DeleteOrder(int id);
        Task<Order?> GetOrder(int id);
    }
}
