﻿using Ecommerce.Entities;

namespace Ecommerce.IRepository
{
    public interface IOrderRepository:IGenericRepository<Order>
    {
    }
}
