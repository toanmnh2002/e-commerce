﻿using Ecommerce.Entities;

namespace Ecommerce.IRepository
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
    }
}
