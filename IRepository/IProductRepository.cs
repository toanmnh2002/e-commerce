﻿using Ecommerce.Enities;

namespace Ecommerce.IRepository
{
    public interface IProductRepository : IGenericRepository<Product>
    {

    }
}
