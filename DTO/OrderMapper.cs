﻿using Ecommerce.Entities;

namespace Ecommerce.DTO
{
    public class OrderMapper
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal TotalPrice { get; set; }
        public int CustomerId { get; set; }
        public ICollection<Order>? Orders { get; set; }
    }
}
