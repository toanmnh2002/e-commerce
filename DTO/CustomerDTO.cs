﻿using Ecommerce.Entities;

namespace Ecommerce.DTO
{
    public class CustomerDTO
    {
        public string FullName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public ICollection<Order>? Orders { get; set; }

    }
}
